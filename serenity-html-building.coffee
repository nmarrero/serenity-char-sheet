serenity = require("./serenity-data.json")

getDNum = (num) ->
  if num < 14
    "d" + num
  else
    num -= 12
    "d12 + " + num

generateStun = (sibling, type, num, title) ->
  for stun in [ 0...num + 1 ] by 1
    $input = $("<input type='radio' />")
    $span = $("<span></span>")
    $input.addClass("sheet-serenity-#{type} sheet-serenity-#{type}#{stun}")
    $input.val(stun)
    $input.attr('title', "#{stun} #{title}")
    $input.attr('name', "attr_#{type}s")
    $input.attr('title', "No #{title}\n(Click to clear)") if stun is 0
    $input.prop('checked', 'checked') if stun is 0
    $input.insertAfter(sibling)
    $span.insertAfter($input)

assets = serenity.ASSETS.sort((asset1, asset2) ->
  asset1name = asset1.name + if asset1.level is "minor" then "0" else "1"
  asset2name = asset2.name + if asset2.level is "minor" then "0" else "1"
  asset1name.localeCompare(asset2name)
)

for item in $('assetsPlaceholder')
  $item = $(item)
  $selectTag = $('<select></select>')
  $selectTag.attr('class', $item.attr('class'))
  $selectTag.attr('name', $item.attr('name'))

  includeOriginals = true
  includeNonOriginals = true
  for asset in assets
    continue if asset.original and not includeOriginals
    continue if not asset.original and not includeNonOriginals
    cost = if asset.level is "minor" then 2 else 4
    cost *= 2 if asset.doubleCost
    asset_description = "#{asset.name} (#{asset.level}) - Cost: #{cost} #{if asset.requiresSpecialization then ' - *Requires Specialization*' else ''}"
    $option = $('<option></option')
    $option.val(asset.description)
    $option.attr('title', asset.description)
    $option.html(asset_description)
    $selectTag.append($option)

  $item.replaceWith($selectTag)

complications = serenity.COMPLICATIONS.sort((complication1, complication2) ->
  complication1name = complication1.name + if complication1.level is "minor" then "0" else "1"
  complication2name = complication2.name + if complication2.level is "minor" then "0" else "1"
  complication1name.localeCompare(complication2name)
)

for item in $('complicationsPlaceholder')
  $item = $(item)
  $selectTag = $('<select></select>')
  $selectTag.attr('class', $item.attr('class'))
  $selectTag.attr('name', $item.attr('name'))

  includeOriginals = true
  includeNonOriginals = true
  for complication in complications
    continue if complication.original and not includeOriginals
    continue if not complication.original and not includeNonOriginals
    cost = if complication.level is "minor" then 2 else 4
    cost *= 2 if complication.doubleCost
    complication_description = "#{complication.name} (#{complication.level}) - Cost: #{cost} #{if complication.requiresSpecialization then ' - *Requires Specialization*' else ''}"
    $option = $('<option></option')
    $option.val(complication.description)
    $option.attr('title', complication.description)
    $option.html(complication_description)
    $selectTag.append($option)

  $item.replaceWith($selectTag)

for item in $('skillDescriptionPlaceholder')
  $item = $(item)
  $skill = $item.attr('name')
  $tooltip = $('<span>' + $skill + '</span>')
  $tooltip.attr('class', 'sheet-serenity-tooltip')

  for skill in serenity.SKILL_DESCRIPTIONS
    continue if skill.name != $skill
    $text = $('<span>' + skill.name + ': ' + skill.intro + '</span>')
    $text.attr('class', 'sheet-serenity-tooltip-text sheet-serenity-tooltip-text-wide')
    $table = $('<table></table>')
    $table.append($('<tr><td>Easy: </td><td>' + skill.Easy + '</td></tr>'))
    $table.append($('<tr><td>Average: </td><td>' + skill.Average + '</td></tr>'))
    $table.append($('<tr><td>Hard: </td><td>' + skill.Hard + '</td></tr>'))
    $table.append($('<tr><td>Formidable: </td><td>' + skill.Formidable + '</td></tr>'))
    $table.append($('<tr><td>Heroic: </td><td>' + skill.Heroic + '</td></tr>'))
    $table.append($('<tr><td>Incredible: </td><td>' + skill.Incredible + '</td></tr>'))
    $table.append($('<tr><td>Ridiculous: </td><td>' + skill.Ridiculous + '</td></tr>'))
    $table.append($('<tr><td>Impossible: </td><td>' + skill.Impossible + '</td></tr>'))
    $details = $('<details><summary>Examples</summary></details>')
    $details.append($table)
    $text.append($details)
    $tooltip.append($text)

  $item.replaceWith($tooltip)

for item in $('skillsPlaceholder')
  $item = $(item)
  $selectTag = $('<select></select>')
  $selectTag.attr('class', $item.attr('class'))
  $selectTag.attr('name', $item.attr('name'))

  for skill in serenity.SKILLS
    $option = $('<option></option>')
    $option.val(skill)
    $option.html(skill)
    $selectTag.append($option)

  $item.replaceWith($selectTag)

for item in $('stunPlaceholder')
  $item = $(item)
  type = $item.attr('type')
  num = parseInt($item.attr('num'), 10)
  title = $item.attr('title')

  generateStun($item, type, num, title)
  $item.remove()

for item in $('dicePlaceholder')
  $item = $(item)
  defaultOpt = parseInt($item.attr('default'), 10)
  start = parseInt($item.attr('start'), 10)
  end = parseInt($item.attr('end'), 10)

  $selectTag = $('<select></select>')
  $selectTag.attr('class', $item.attr('class'))
  $selectTag.attr('name', $item.attr('name'))

  for opt in [ start...end + 1 ] by 2
    $option = $('<option></option')
    $option.val(opt)
    $option.html(getDNum(opt))
    $option.prop('selected', true) if opt is defaultOpt
    $selectTag.append($option)

  $item.replaceWith($selectTag)

for item in $('damageTypePlaceholder')
  $item = $(item)

  $selectTag = $('<select></select>')
  $selectTag.attr('class', $item.attr('class'))
  $selectTag.attr('name', $item.attr('name'))

  strings = {'B': 'Basic', 'S': 'Stun', 'W': 'Wound'}
  for opt in ['B', 'S', 'W']
    $option = $('<option></option')
    $option.val(opt)
    $option.html(strings[opt])
    $option.prop('selected', true) if opt is 'B'
    $selectTag.append($option)

  $item.replaceWith($selectTag)