const jsdom = require('jsdom');
const fs = require('fs');

const inputHtml = fs.readFileSync('build/serenity.in.html', 'utf8');
const { window } = new jsdom.JSDOM(inputHtml);
global.$ = require('jquery')(window);

require("./build/serenity-html-building");
require("./build/serenity-roll20-check");

const sheet = window.document
    .querySelector('.sheet-serenity-charactersheet')
    .outerHTML
    .replace(/€/g, "&euro;");
fs.writeFileSync('build/serenity.html', sheet);


